package ru.mike.testmodule

import android.widget.TextView
import androidx.annotation.Keep
import de.robv.android.xposed.*
import de.robv.android.xposed.callbacks.XC_LoadPackage

@Keep
class XposedMod : IXposedHookLoadPackage {

    // adb -s 5b58b2a1 shell am set-debug-app -w --persistent ru.mike.testapplication
    // adb -s 5b58b2a1 shell am clear-debug-app ru.mike.testapplication

    companion object {
        const val PREF_NAME = "pref_file"
        const val PREF_KEY = "key_int"
    }

    @Keep
    override fun handleLoadPackage(lpp: XC_LoadPackage.LoadPackageParam) {
        if (lpp.packageName == "ru.mike.testapplication") {
            foo(lpp)
        }
    }

    private fun foo(lpp: XC_LoadPackage.LoadPackageParam) {
        val pref = try {
            if (XposedBridge.getXposedVersion() >= 93)
                XSharedPreferences(BuildConfig.APPLICATION_ID, PREF_NAME)
            else
                XSharedPreferences(BuildConfig.APPLICATION_ID)
        } catch (t: Throwable) {
            null
        }

        val value = pref?.getInt(PREF_KEY, 0) ?: 0
        if (value > 0) {
            XposedHelpers.findAndHookMethod("ru.mike.testapplication.MainActivity", lpp.classLoader,
                                            "foo", TextView::class.java, object : XC_MethodHook() {
                    override fun beforeHookedMethod(param: MethodHookParam?) {
                        (param?.args?.getOrNull(0) as? TextView)?.let {
                            it.text = "" + value
                        }
                    }
                })
        }
    }

}