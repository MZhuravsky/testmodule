package ru.mike.testmodule

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.io.File
import java.lang.reflect.Field

class MainActivity : AppCompatActivity() {

    private lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textView = findViewById<TextView>(R.id.textView)
        val editText = findViewById<EditText>(R.id.editText)
        val button = findViewById<Button>(R.id.button)
        button.setOnClickListener {
            editText.text.toString().toIntOrNull()?.let {
                updatePrefs(it)
            }
        }
    }

    @SuppressLint("WorldReadableFiles")
    private fun updatePrefs(value: Int) {
        val prefs = try {
            getSharedPreferences(XposedMod.PREF_NAME, Context.MODE_WORLD_READABLE)
        } catch (e: SecurityException) {
            null
        }
        prefs?.edit()?.putInt(XposedMod.PREF_KEY, value)?.apply()

        textView.text = getSharedPrefsPath(prefs)
    }

    fun getSharedPrefsPath(prefs: Any?): String {
        return if (prefs != null) {
            try {
                val fFile: Field = prefs.javaClass.getDeclaredField("mFile")
                fFile.isAccessible = true
                (fFile.get(prefs) as File).parentFile.absolutePath
            } catch (t: Throwable) {
                "mFile error"
            }
        } else "no prefs"
    }
}